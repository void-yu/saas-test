import copy
def gen_comment_key(d):
    if type(d)!=dict and type(d)!=list: return
    if type(d)==dict:
        kl=[k for k in d]

        for k in kl:
            d[k+'_']=''
            gen_comment_key(d[k])

    if type(d)==list:
        for o in d:
            gen_comment_key(o)


def clear_dict(d,remain='data'):
    if type(d)!=dict and type(d)!=list: return
    if type(d)==dict:
        kl=[k for k in d]
        for k in kl:
            if remain=='data' and k[-1]=='_': del d[k]
            if remain=='comment' and k[-1]!='_': del d[k]

        kl=[k for k in d]

        for k in kl:
            clear_dict(d[k])

    if type(d)==list:
        for o in d:
            clear_dict(o)

def get_clear_dict(d,remain):
    r=copy.deepcopy(d)
    clear_dict(r,remain)
    return r
