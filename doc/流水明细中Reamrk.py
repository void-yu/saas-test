

remark={
    #开票
    "invoice_receiver_name":"广东深华消防","invoice_receiver_name_":"收票单位",
    #回款
    "payer_name":"广东深华消防","payer_name_":"付款方全称",
    #"payee_account":"中国银行艺园路支行23123934945","payee_account_":"收款帐号",

    #付款
    "payee_name":"广东深华消防","payee_name_":"收款人",
    "payee_account":"中国银行艺园路支行23123934945","payee_account_":"收款人帐号",

    #扣款-平台服务费
    "return_amount":"712343.23","return_amount_":"回款",
    "deduct_ratio":"23.43","deduct_ratio_":"扣款比例",

    #扣款-人工扣款
    "description":"借款本金扣除的本金","description_":"说明",
}