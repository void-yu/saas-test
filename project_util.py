# -*- coding: utf-8 -*-
from util import *
from config import Config
from werkzeug.wrappers import *
from flask import *
from config import *
#_logger=logging.getLogger()





def itf(is_public=1):
    def interface(f):
        @wraps(f)
        def interface_wrap(*args,**kwargs):
            r,code={},200


            with DBConnection(DBPOOL) as con:
                data='{}'
                if request.data.__len__()!=0:data=request.data.decode('utf-8')
                elif 'data' in request.args:data=request.args['data']
                elif 'data' in request.form:data=request.form['data']

                kwargs['data']=json.loads(data)
                cr=con.cursor()
                kwargs.update({'conn':con,'cr':cr})

                if CF._ENV=='doc' and request.path not in ['/query__doc','/update__doc']:
                    doc_list=cr.list("SELECT * FROM doc WHERE url='%s'"%request.path)

                    r=copy.deepcopy(doc_list[0]['response_example'])

                    for i in range(doc_list.__len__()):

                        o=doc_list[i]
                        o['response_data_json']=get_clear_dict(o['response_json'],'data')
                        o['response_comment_json']=get_clear_dict(o['response_json'],'comment')
                        del o['response_example']


                    r['doc_list']=doc_list


                    pass
                else:
                    r=f(*args,**kwargs)


            if r==None:r={}
            indent=4 if CF._ENV!='product' else 0
            return json.dumps(r,ensure_ascii=0,indent=indent),code

        return interface_wrap
    return interface

