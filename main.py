# -*- coding: utf-8 -*-
from flask import *
import json
import traceback,logging
import uuid,time
from config import DBPOOL
from db import DBConnection
from util import *
from logging import getLogger
from functools import wraps
from flask_cors import *
import random
from bp.basic_bp import basic_bp
from bp.msg_bp import msg_bp
from bp.risk_bp import risk_bp


#initLog()
app = Flask(__name__)
app.register_blueprint(basic_bp)
app.register_blueprint(msg_bp)
app.register_blueprint(risk_bp)

CORS(app,supports_credentials=1)

def init_db():
    with DBConnection(DBPOOL) as db:
        with app.open_resource('init.sql', mode='r') as f:
            db.cursor().execute(f.read())
        db._con.commit()

@app.cli.command('initdb')
def initdb_command():
    init_db()
    print('初始化数据表和视图')

if __name__=='__main__':
    #app.run(host='0.0.0.0',processes=3)
    app.run(host='0.0.0.0',port=5000,threaded=1)
