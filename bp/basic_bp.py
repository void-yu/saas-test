from config import *
from flask import *
from project_util import *

basic_bp=Blueprint('basic_bp',__name__)



@basic_bp.route('/query__doc')
@itf(is_public=0)
def query__doc(**kw):
    data,cr=kw['data'],kw['cr']
    w=' WHERE 1=1 '
    if 'name' in data:w+="AND name='%s' "%data['name']
    if 'cat' in data: w+="AND cat='%s' "%data['cat']
    if 'url' in data: w+="AND url='%s' "%data['url']


    r=cr.list("SELECT * FROM t_doc %s"%w)

    for i in range(r.__len__()):
        o=r[i]
        o['response_data_json']=get_clear_dict(o['response_json'],'data')
        o['response_comment_json']=get_clear_dict(o['response_json'],'comment')


    return r


@basic_bp.route('/update__doc')
@itf(is_public=0)
def update__doc(**kw):
    data,cr=kw['data'],kw['cr']
    w=' WHERE 1=1 '
    if 'name' in data: w+="AND name='%s' "%data['name']
    if 'cat' in data: w+="AND cat='%s' "%data['cat']
    if 'url' in data: w+="AND url='%s' "%data['url']

    r=cr.execute("UPDATE t_doc SET response_example='%s' %s"%(d2j(data['response_example']),w))

    return {}


@basic_bp.route('/login',methods=['POST','GET'])
@itf(is_public=0)
def login(**kw):
    data,cr=kw['data'],kw['cr']
    print(request)
    pass

@basic_bp.route('/query__contract',methods=['POST','GET'])
@itf(is_public=0)
def query__contract(**kw):
    data,cr=kw['data'],kw['cr']
    print(request)
    pass


