drop view if exists tbl_contract_messages;

drop view if exists tbl_message_details;

drop table if exists tbl_message_status;
create table tbl_message_status (
    id serial PRIMARY KEY,
    bm_id integer not null,
    last_message_id integer not null
)
